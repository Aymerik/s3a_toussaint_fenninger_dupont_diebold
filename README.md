#  Comment installer l'application mywishlist sur le serveur de votre choix

##  I) Cloner le dépôt
Tout d'abord, il convient de **récupérer une copie du dépôt ** via ssh ou https par le biais de git.

## II) Mise en place des frameworks nécessaires

Pour cela, il faut d'abord avoir installé composer et une version récente de php,
puis se déplacer à la racine du projet (là où se trouve le fichier index.php),
et exécuter la commande **composer install** .
le dossier du projet doit quand à lui être accessible par le service web que l'on souhaite utiliser 

Pour l'installation de l'application sur des systèmes GNU/Linux, et avec un service web apache2,
il convient d'éditer un fichier .htaccess à la racine du projet permettant la réécriture des URLs.
Dans le cas où ceci n'est pas effectué, la configuration d'apache pourrait poser des problèmes et 
bloquer le bon fonctionnement de l'application.

Par ailleurs, le module php d'apache doit être activé grâce à la commande
**sudo a2enmod libapache-php7.0**
Si le module n'est pas présent, vous pouvez l'installer grâce à votre gestionnaire de paquets favori.

## III) Préparation de la base de données avec un jeu de test ( Optionnel )

Au préalabble, il est nécessaire de crée un fichier **db.ini** dans le dossier **src/conf/** contenant les informations de la base de données
ainsi que les identifiants
Ensuite, munissez vous de votre client d'accès à une base de données favoris, et exécutez-y,
sur une nouvelle base de données, l'ensemble des commandes présentes danas le fichier 
**doc/wishlist.sql**


Dans l'éventualité où vous souhaitez simplement créer la structure de la base de données,
n'exécutez pas les commandes d'insertions présentes dans le fichier

## IV) Profitez !
Votre copie de l'application mywishlist est désormais prête et fonctionnelle !
C'est à vous d'effectuer ce que vous voulez avec !
