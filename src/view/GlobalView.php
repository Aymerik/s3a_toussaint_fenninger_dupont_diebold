<?php
/**
 * Created by PhpStorm.
 * User: aymer
 * Date: 12/12/17
 * Time: 11:47
 */

namespace mywishlist\view;

class GlobalView extends AbstractView
{
    private $data;
    const AFF_HOME = 0;
    const AFF_CONNEXION = 1;
    const AFF_LISTE = 2;
    const AFF_ITEM = 3;
    const AFF_RESERVATION = 4;
    const AFF_REGISTER = 5;
    const AFF_CREER_LISTE = 6;
    const AFF_MODIFIER_LISTE = 7;
    const AFF_PROFILE = 8;
    const AFF_MES_LISTES = 9;
    const AFF_PUBLIC_LISTES = 10;
    const AFF_CREATORS = 11;

    public function __construct($data = ""){
      parent::__construct();
      $this->data = $data;
    }
	public function render($sel = "")
    {
        switch ($sel) {
            case self::AFF_HOME:
                $content = (new HomeView())->render();
                break;
            case self::AFF_CONNEXION:
                $content = (new ConnectingView())->render();
                break;
            case self::AFF_REGISTER:
                $content = (new RegisterView())->render();
                break;
            case self::AFF_CREER_LISTE:
                $content = (new NewListView())->render();
                break;
            case self::AFF_LISTE:
                $content = (new ListView())->render($this->data);
                break;
            case self::AFF_MODIFIER_LISTE:
                $content = (new UpdateListView())->render($this->data);
                break;
            case self::AFF_ITEM:
                $content = (new ItemView())->render($this->data);
                break;
            case self::AFF_PROFILE:
                $content = (new ProfileView())->render($this->data);
                break;
            case self::AFF_MES_LISTES:
                $content = (new MyListView())->render($this->data);
                break;
            case self::AFF_PUBLIC_LISTES:
                $content = (new PublicListView())->render($this->data);
                break;
            case self::AFF_CREATORS:
                $content = (new CreatorsView())->render($this->data);
                break;
            default:
                $content = "Vue introuvable";
                break;
        }

        return self::afficheBase($content);
    }


    public function afficheBase($content){
        $linkPublic = $this->app->urlFor("list_pub");
        $linkConnexion = $this->app->urlFor("connexion");
        $linkHome = $this->app->urlFor("accueil");
        $linkRegister = $this->app->urlFor("register");
        $linkDeconnexion = $this->app->urlFor("deconnexion");
        $linkProfile = $this->app->urlFor('profile');
        $linkMyLists = $this->app->urlFor('mesListes');
        $linkCreators = $this->app->urlFor('creators');
        if(isset($_SESSION['user'])){
            $linkCreate = $this->app->urlFor("creer_liste");
        }else{
            $linkCreate = $this->app->urlFor("connexion");
        }

        $msgs = $_SESSION['slim.flash']['message'] ?? '';
        $return = <<<END
        <!DOCTYPE html>
        <html lang="fr">

        <head>

            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>My WishList</title>

<!-- Bootstrap core CSS -->
<link href="{$this->rootUrl}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template -->
<link href="{$this->rootUrl}/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

<!-- Custom styles for this template -->
<link href="{$this->rootUrl}/assets/css/agency.css" rel="stylesheet">
<link href="{$this->rootUrl}/assets/css/main.css" rel="stylesheet">

<link rel="icon" type="image/png" href="{$this->rootUrl}/imgBank/favicon.ico" />

</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
  <div class="container">
    <a class="navbar-brand js-scroll-trigger" href="$linkHome">My Wishlist</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      Menu
      <i class="fa fa-bars"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav text-uppercase ml-auto">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="$linkPublic">Listes publiques</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="$linkCreators">Créateurs</a>
        </li>
END;
        if(isset($_SESSION['user']))
        {
            $return .= '<li class="nav-item">
           <a class="nav-link js-scroll-trigger" href='.$linkProfile.'>Mon compte</a>
          </li>
          <li class="nav-item">
           <a class="nav-link js-scroll-trigger" href='.$linkMyLists.'>Mes listes</a>
         </li>\';
           <li class="nav-item">
           <a class="nav-link js-scroll-trigger" href="'.$linkDeconnexion.'">Déconnexion</a>
         </li>';
        }else{
            $return .= "<li class=\"nav-item\">
           <a class=\"nav-link js-scroll-trigger\" href=\"".$linkConnexion."#connecting"."\">Connexion</a>
          </li>
         <li class=\"nav-item\">
           <a class=\"nav-link js-scroll-trigger\" href=\"".$linkRegister."#register"."\">Inscription</a>
         </li>";
        }
        $return .= <<<END

      </ul>
    </div>
  </div>
</nav>

<!-- Header -->
<header class="masthead">
  <div class="container">
    <div class="intro-text">
      <div class="intro-lead-in">Bienvenue sur My WishiList!</div>
      <div class="intro-heading text-uppercase">Les listes de vos rêves !</div>
      <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href={$linkCreate}>Créer une liste</a>
    </div>
  </div>
</header>
<div class="row">
    <div class="col-md-12 alert-info">
        $msgs
    </div>
</div>

$content

<!-- Footer -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <span class="copyright">Copyright &copy; My WishList 2017 - 2018 | Aymerik D, Thomas F, Guillaume T, Alexandre D</span>
      </div>
    </div>
  </div>
</footer>


<!-- Bootstrap core JavaScript -->
<script src="{$this->rootUrl}/assets/vendor/jquery/jquery.min.js"></script>
<script src="{$this->rootUrl}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="{$this->rootUrl}/assets/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Contact form JavaScript -->
<script src="{$this->rootUrl}/assets/js/jqBootstrapValidation.js"></script>

<!-- Custom scripts for this template -->
<script src="{$this->rootUrl}/assets/js/agency.min.js"></script>

<!-- KojimaCode -->
<script src ="{$this->rootUrl}/assets/js/konamiCode.js"></script>

</body>

</html>



END;
        return $return;
    }
}