<?php
/**
 * Created by PhpStorm.
 * User: redma
 * Date: 13/01/2018
 * Time: 20:25
 */

namespace mywishlist\view;


class ProfileView extends AbstractView{

    public function render($user){
        $app = \Slim\Slim::getInstance();
        $linkProfile = $app->urlFor("profile");
        $linkDelete = $app->urlFor("delete");
        return <<<END
<section class="modifyprofile">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Modifier son compte</h2>
                <form action"$linkProfile" method="post">
                <div class="form-group">
                    <label for="mail">Nom de compte</label>
                    <input type="text" class="form-control" id="username" name="username" value="$user->username" placeholder="Entrez votre nouveau nom d'utilisateur" autofocus>
                </div>
                <div class="form-group">
                    <label for="password">Nouveau mot de passe</label>
                    <input type="password" class="form-control" id="newpassword" name="newpassword" placeholder="Laissez vide si vous ne voulez pas le changer">
                    <p id="help-pass"></p>
                </div>
                <div class="form-group">
                    <label for="password">Confirmez votre nouveau mot de passe</label>
                    <input type="password" class="form-control" id="newpasswordvalid" name="newpasswordvalid" placeholder="Re-entrez votre nouveau mot de passe">
                </div>
                <div class="form-group">
                    <label for="password">Ancien mot de passe</label>
                    <input type="password" class="form-control" id="oldpassword" name="oldpassword" placeholder="Entrez votre ancien mot de passe">
                </div>
                <button type="submit" class="btn btn-primary" name="connexion" value="connect">Modifier mes informations de compte</button>
                </form>
            </div>
        </div>
    </div>
</section>
<section class="deleteProfile" style="background-color: #212529;color: lightgray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Supprimer mon compte</h2>
                <div class="form-group">
                <button onclick="confirmation()" type="submit" class="btn btnSuppr">Supprimer mon compte</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    document.getElementById('newpassword').addEventListener('input',function(){
        if(document.getElementById('newpassword').value.length < 7)
            document.getElementById("help-pass").textContent = "Le mot de passe doit au moins faire 7 caractères"
        else
            document.getElementById("help-pass").textContent = ""
    });
function confirmation(){
    var r = confirm("Voulez vous vraiment supprimer votre compte ? Ceci sera définitif et vous ne pourrez plus accéder à celui-ci");
    if(r==true){
        window.location.href = "$linkDelete";
    }
}
</script>
END;
    }
}
