<?php
/**
 * Created by PhpStorm.
 * User: redma
 * Date: 14/01/2018
 * Time: 21:38
 */

namespace mywishlist\view;


use mywishlist\model\WishList;

class MyListView extends AbstractView
{
    public function render($data){
        $linkCreate = $this->app->urlFor('creer_liste');
        $data = unserialize($data);
        $id = $data->id;
        $allList = WishList::where('user_id','=',$id)->orderBy('deadline','desc')->get();
        $listes = []; $expired = [];
        $time = time();
        foreach ($allList as $list){
            if(strtotime($list->deadline) < $time){
                $expired[] = $list;
            }else{
                $listes[] = $list;
            }
        }
        $return = <<<END
<section class="mylist">
<div class="container">
<div class="row">
<div class="col-lg-12 text-center">
END;

        if(count($listes) > 0){
            $return = $return."<h3>Mes listes</h3><br>";
            foreach ($listes as $list){
                $style = "normalMyList";
                $time = date('d/m/Y',strtotime($list->deadline));
                $return = $return.<<<END
<a class="mylistlink" href="{$this->app->urlFor('liste',['id'=>$list->id,'token'=>$list->token])}">
<div class="$style">
    <h5>$list->title expire le : $time</h5>
    <p>$list->description</p>
</div></a>
<hr/>
END;
            }
        }if(count($expired) > 0){
            $return = $return."<h3>Mes listes expirées</h3><br>";
            foreach ($expired as $list) {
                $style = "expiredMyList";
                $time = date('d/m/Y',strtotime($list->deadline));
                $return = $return . <<<END
<a class="mylistlink" href="{$this->app->urlFor('liste', ['id' => $list->id, 'token' => $list->token])}">
<div class="$style">
    <h5>$list->title a expiré le : $time</h5>
    <p>$list->description</p>
</div></a>
<hr/>
END;
            }
        } if(count($listes)+count($expired) < 0){
            $return = $return."<div>Oops, il semblerais que vous n'ayez jamais créé de liste</div>";
        }
    return $return.<<<END
    <h3><a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="$linkCreate">Créer une nouvelle liste</a></h3>
    </div>
    </div>
    </div>
</section>
END;
    }
}