<?php
/**
 * Une classe abstraite pour factoriser le code (pour les url, ...)
 */
namespace mywishlist\view;

abstract class AbstractView {
	protected $app;
	protected $rootUrl;

	public function __construct(){
		$this->app = \Slim\Slim::getInstance();
		$this->rootUrl = str_replace("/index.php","",$this->app->request->getRootUri());
	}
}