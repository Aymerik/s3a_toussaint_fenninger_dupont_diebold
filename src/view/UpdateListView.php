<?php
/**
 * Created by PhpStorm.
 * User: aymer
 * Date: 20/12/2017
 * Time: 16:45
 */

namespace mywishlist\view;


class UpdateListView extends AbstractView
{
    public function render($liste)
    {
        $return = <<<END
    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Modifier {$liste->title}</h2>
          </div>
        </div>
<form method="POST" action="{$this->app->urlFor('modifier_liste_valider', ['id' => $liste->id])}">
  <div class="form-group">
    <label for="name">Nom de la liste</label>
    <input type="texte" class="form-control" id="name" name="name" placeholder="Mariage de Alice et Bob" value="{$liste->title}">
  </div>
  <div class="form-group">
    <label for="description">Description</label>
    <textarea class="form-control" id="description" name="description">{$liste->description}</textarea>
  </div>
  <div class="form-group">
    <label for="deadline">Fin de validité</label>
    <input type="date" class="form-control" id="deadline" name="deadline" value="{$liste->deadline}">
  </div>
    <div class="form-check">
        <label for="isPublic">Liste publique ?</label>
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" id="isPublic" name="isPublic" value="1" 
END;
        if ($liste->isPublic) {
            $return .= "checked";
        }
        $return .= <<<END
>
         Oui
        </label>
  </div>
  <button type="submit" class="btn btn-primary" name="submit" value="create">Modifier la liste</button>
</form>
</div>
</section>
END;
        return $return;
    }
}