<?php
/**
 * Created by PhpStorm.
 * User: aymer
 * Date: 12/12/17
 * Time: 11:47
 */

namespace mywishlist\view;

class ListView extends AbstractView
{
    public function render($liste){
      $deadline = date('d/m/Y', strtotime($liste->deadline));
      $items = "";
      foreach($liste->items as $item)
      {
          $items .= '<h4><a href="'.$this->app->urlFor("item", ["token" => $liste->token, "item" => $item->id]).'">'.$item->name.'</a></h4><img src="'.$item->images.'" alt="item" /><p>'.$item->description.', <strong><em>'.$item->price.'€</em></strong><br /></p>';
      }
        $return = <<<END
    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">{$liste->title}</h2>
            <h3 class="section-subheading text-muted">{$liste->description} - arrive à échéance le {$deadline}<br />
END;

        if(isset($_SESSION['user']) && $liste->user_id == unserialize($_SESSION['user'])->id)
        {
            $return .= '<a href="'.$this->app->urlFor('editer_liste', ['id' => $liste->id]).'">Modifier</a> - <a href="'.$this->app->urlFor('supprimer_liste', ['id' => $liste->id]).'">Supprimer</a>';
        }

            $return .= <<<END
            </h3>
          </div>
        </div>
  <ul class="nav nav-pills">
    <li class="active"><a data-toggle="pill" href="#items">Items</a></li>
    <li><a data-toggle="pill" href="#comments">Commentaires</a></li>
  </ul>
  

<div class="tab-content">
  <div id="items" class="tab-pane fade in active show">
    <h3>Items</h3>
    $items
END;
      if(isset($_SESSION['user']) && unserialize($_SESSION['user'])->id == $liste->user_id) {

          $return .= <<<END
<form action="{$this->app->urlFor('ajouter_item', ['id' => $liste->id])}" method="POST" id="addItem" enctype="multipart/form-data">
                      <div class="form-group">
    <label for="name">Nom</label>
    <input type="text" class="form-control" id="name" name="name" placeholder="Nom de l'item">
  </div>
  <div class="form-group">
    <label for="price">Prix</label>
    <input type="number" step="0.1" class="form-control" id="price" name="price" placeholder="Coût de l'item">
  </div>
  <div class="form-group">
    <label for="description">Description</label>
    <textarea class="form-control" id="description" name="description" placeholder="Décrivez votre item"></textarea>
  </div>
  <div class="form-group">
    <label for="image">Image</label>
    <div id="contentIMG">
    <input type="text" class="form-control" id="image" name="image" placeholder="Lien vers l'icône de l'item">
    </div>
    <p><a href="#" id="changeTypeImg">Je préfère uploader une image</a></p>
  </div>
  <script type="text/javascript">
    document.getElementById("changeTypeImg").addEventListener('click', function(e){
        e.preventDefault();
        document.getElementById("changeTypeImg").textContent = (document.getElementById("changeTypeImg").textContent == "Je préfère uploader une image") ? "Je préfère passer par une URL" : "Je préfère uploader une image";
        document.getElementById("contentIMG").innerHTML = (document.getElementById("changeTypeImg").textContent == "Je préfère uploader une image") ? '<input type="text" class="form-control" id="image" name="image" placeholder="Lien vers l\'icône de l\'item">' : '<input type="file" class="form-control" id="image" name="image" placeholder="Uploader l\'image de l\'item">';
    });
</script>
<div class="form-group">
    <label for="link">Lien <em>(facultatif)</em></label>
    <input type="text" class="form-control" id="link" name="link" placeholder="Lien externe pour compléter l'item ?">
  </div>
  <button type="submit" class="btn btn-primary" name="register" value="addItem">Ajouter l'item</button>
                </form>
END;
      }

     $return .= <<<END
  </div>
  <div id="comments" class="tab-pane fade">
    <h3>Commentaires</h3>
END;
      if(count($liste->comments) == 0)
      {
          $return .= '<p style="font-style: italic;">Pas de commentaire</p>';
      }
      foreach($liste->comments as $comment){
          $return .= '<div class="comment"><strong>'.$comment->author.' a écrit :</strong><p>'.$comment->content.'</p><p style="text-align: right; font-style: italic;">Posté le '.date('d/m/Y \à H\hi', strtotime($comment->created_at)).'</p></div>';
      }
        $username = (isset($_SESSION['user'])) ? unserialize($_SESSION['user'])->username : '';
        $return .= <<<END
      <h4>Ajouter un commentaire</h4>
      <form action="{$this->app->urlFor('commenter_liste', ['id' => $liste->id, 'token' => $liste->token])}" method="POST">
                      <div class="form-group">
    <label for="author">Qui êtes vous ?</label>
    <input type="text" class="form-control" id="author" name="author" placeholder="Nom prénom" value="$username">
  </div>
  <div class="form-group">
    <label for="content">Commentaire</label>
    <textarea class="form-control" id="content" name="content" placeholder="Qu'avez-vous à dire ?"></textarea>
  </div>
  <button class="btn btn-primary" name="comment" value="comment">Publier</button>
                </form>
  </div>

</div>
               
            </div>           

    </section>

END;
      return $return;
    }
}