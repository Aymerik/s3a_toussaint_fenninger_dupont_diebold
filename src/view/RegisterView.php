<?php
/**
 * Created by PhpStorm.
 * User: redma
 * Date: 20/12/2017
 * Time: 16:45
 */

namespace mywishlist\view;


class RegisterView extends AbstractView
{
    public function render(){
        return <<<END
    <section id="register">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Inscription</h2>
          </div>
        </div>
<form method="POST" action="{$this->app->urlFor('register')}">
  <div class="form-group">
    <label for="mail">Adresse e-mail</label>
    <input type="email" class="form-control" id="mail" name="mail" placeholder="Entrez votre adresse mail" autofocus>
  </div>
  <div class="form-group">
    <label for="username">Pseudo</label>
    <input type="text" class="form-control" id="username" name="username" placeholder="Choisissez un pseudo">
  </div>
  <div class="form-group">
    <label for="password">Mot de passe</label>
    <input type="password" class="form-control" id="password" name="password" placeholder="Entrez votre mot de passe">
    <p id="help-pass"></p>
    <script type="text/javascript">
        document.getElementById('password').addEventListener('input',function(){
            if(document.getElementById('password').value.length < 7)
                document.getElementById("help-pass").textContent = "Le mot de passe doit au moins faire 7 caractères"
            else
                document.getElementById("help-pass").textContent = ""
        });
    </script>
  </div>
  <div class="form-group">
    <label for="confirm">Retapez votre mot de passe</label>
    <input type="password" class="form-control" id="confirm" name="confirm" placeholder="Juste pour être sûr...">
  </div>
  <button type="submit" class="btn btn-primary" name="register" value="register">Inscription</button>
</form>
</div>
</section>
END;
    }
}