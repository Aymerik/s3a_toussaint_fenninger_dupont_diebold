<?php
/**
 * Created by PhpStorm.
 * User: redma
 * Date: 01/01/2018
 * Time: 21:19
 */

namespace mywishlist\view;


use mywishlist\model\WishList;

class ItemView extends AbstractView
{
    public function render($data)
    {
        $idList = $data['idList'];
        $item = unserialize($data["item"]);
        $token = $data["token"];

        $htmlItem = self::afficherItem($item,$token,$idList);
        $return = <<<END
<section class="itemview">
    <div class="container">
        <div class="row">
            <a href="{$this->app->urlFor('liste', ['id' => $idList, 'token' => $token])}" class="btn btn-primary">Retour à la liste</a>
            $htmlItem
        </div>
    </div>
</section>
<section class="">
    <div class="container">
        <div class="row">
            
        </div>
    </div>
</section>
END;
        if (is_null($item->booking_id)) {
            $return .= '<div class="row"><div class="col-lg-12"><form action="' . $this->app->urlFor("book_item", ['id' => $item->id]) . '" method="POST">
                      <div class="form-group">
    <label for="name">Qui êtes vous ?</label>
    <input type="text" class="form-control" id="name" name="name" placeholder="Nom prénom" value="';

            if (isset($_SESSION['user'])) {
                $return .= unserialize($_SESSION['user'])->username;
            }
            $return .= '">
  </div>
  <div class="form-group">
    <label for="comment">Voulez-vous laisser un commentaire ?</label>
    <textarea class="form-control" id="comment" name="comment" placeholder=""></textarea>
  </div>
  <input type="hidden" name="token" value="' . $token . '" />
  <input type="hidden" name="id_list" value="' . $idList . '" />
  <button class="btn btn-primary" name="book" value="bookItem">Réserver</button>
                </form></div></div>';
        } else {
            $return .= '<p>Réservé par <strong>' . $item->booking->name . '</strong> le ' . date('d/m/Y', strtotime($item->booking->created_at)) . '</p>';
        }
        $return .= '</div></section>';

        return $htmlItem;
    }

    private function afficherItem($item,$token,$idList){
        $reserv =self::reserv($item,$token,$idList);
        $modif = "";
        if(strlen($item->images)>0){
            $image ='<img src="'.$item->images.'" alt="'.$item->name.'" height="60" width="60">';
        }
        if(is_null($item->book_id) && isset($_SESSION['user']) && unserialize($_SESSION['user'])->id == WishList::where('id','=',$idList)->first()->user_id && is_null($item->booking_id)){
            $modif = self::afficheModifier($item,$token,$idList);
        }
        $return = <<<END

<section class="itemview">
    <div class="container">
    <a href="{$this->app->urlFor('liste', ['id' => $idList, 'token' => $token])}" class="btn btn-primary">Retour à la liste</a>
        <div class="row">
            <div class="col">$image</div>
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">{$item->name}</h2>
                <h3 class="section-subheading text-muted">{$item->description} </h3>
                $modif
            </div>
        </div>
    </div>
</section>
$reserv
END;
        return $return;
    }

    public function afficheModifier($item,$token,$idList){
        $linkModify = $this->app->urlFor('modif_item',['id'=>$item->id]);
        $linkDelete = $this->app->urlFor('suppr_item',['id'=>$item->id,'idList'=>$idList,'token'=>$token]);
        $linkDeleteIMG = $this->app->urlFor('suppr_img_item',['id'=>$item->id,'idList'=>$idList,'token'=>$token]);
        return <<<END
<form action="$linkModify" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <label for="name">Nom</label>
        <input type="text" class="form-control" id="name" name="name" value="$item->name" placeholder="Nom de l'item">
    </div>
    <div class="form-group">
        <label for="price">Prix</label>
        <input type="number" step="0.1" class="form-control" id="price" value="$item->price" name="price" placeholder="Coût de l'item">
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <input type="text" class="form-control" id="description" name="description" value="$item->description" placeholder="Décrivez votre item">
    </div>
    
    <div class="form-group">
        <label for="image">Changer l'image <em>(laissez vide pour ne pas modifier)</em></label>
            <div id="contentIMG">
    <input type="text" class="form-control" id="image" name="image" placeholder="Lien vers l'icône de l'item">
    </div>
    <p><a href="#" id="changeTypeImg">Je préfère uploader une image</a></p>
  </div>
  <script type="text/javascript">
    document.getElementById("changeTypeImg").addEventListener('click', function(e){
        e.preventDefault();
        document.getElementById("changeTypeImg").textContent = (document.getElementById("changeTypeImg").textContent == "Je préfère uploader une image") ? "Je préfère passer par une URL" : "Je préfère uploader une image";
        document.getElementById("contentIMG").innerHTML = (document.getElementById("changeTypeImg").textContent == "Je préfère uploader une image") ? '<input type="text" class="form-control" id="image" name="image" placeholder="Lien vers l\'icône de l\'item">' : '<input type="file" class="form-control" id="image" name="image" placeholder="Uploader l\'image de l\'item">';
    });
</script>
    <div class="form-group">
        <label for="link">Lien <em>(facultatif)</em></label>
        <input type="text" class="form-control" id="link" name="link" value="$item->link" placeholder="Lien externe pour compléter l'item ?">
    </div>
    <input type="hidden" name="token" value="$token" />
    <input type="hidden" name="id_list" value="$idList" />
    <input type="hidden" name="id_item" value="$item->id" />
    <button class="btn btn-primary" name="book" value="bookItem">Modifier</button>
</form>
    <a href="$linkDelete" class="btn btn-danger" style="margin-top: 1em">Supprimer</a>
    <a href="$linkDeleteIMG" class="btn btn-danger" style="margin-top: 1em">Supprimer l'image</a>
END;
    }

    private function reserv($item,$token,$idList){
        if(WishList::where('id','=',$idList)->first()->isFinished()){
            $content = self::afficherExpireReserv($item);
        }else if(isset($_COOKIE['liste']) && $_COOKIE['liste']=='c'){
            $content = self::afficheReservCreat($item);
        }else if(!is_null($item->booking_id)){
            $content = self::afficherReservInvite($item);
        }else{
            $content = self::afficherFormReserv($item,$token,$idList);
        }
        return <<<END
<section class="reserv">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            $content
            </div>
        </div>
    </div>
</section>
END;

    }

    private function afficheReservCreat($item){
        return '<p><h4>Réservé le' . date('d/m/Y', strtotime($item->booking->created_at)) . '</h4></p>';
    }

    private function afficherReservInvite($item){
        return '<p><h4>Réservé par <strong>' . $item->booking->name . '</strong></h4> le ' . date('d/m/Y', strtotime($item->booking->created_at)) . '</p>';
    }

    private function afficherExpireReserv($item){
        if(is_null($item->booking_id)){
            $content = '<p><strong>Ce objet n\'a pas été réservé</strong></p>';
        }else{
            $content = self::afficherReservInvite($item);
        }
        return $content;
    }

    private function afficherFormReserv($item,$token,$idList){
        $linkBooking = $this->app->urlFor("book_item", ['id' => $item->id]);
        $username = "";
        if(isset($_SESSION['user'])){
            $username = unserialize($_SESSION['user'])->username;
        }
        $return = <<<END
<form action="$linkBooking" method="POST">
    <div class="form-group">
        <label for="name">Qui êtes vous ?</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Nom prénom" value="$username">
    </div>
    <div class="form-group">
        <label for="comment">Voulez-vous laisser un commentaire ?</label>
        <textarea class="form-control" id="comment" name="comment" placeholder=""></textarea>
    </div>
    <input type="hidden" name="token" value="$token" />
    <input type="hidden" name="id_list" value="$idList" />
    <button class="btn btn-primary" name="book" value="bookItem">Réserver</button>
</form>
END;
        return $return;
    }
}