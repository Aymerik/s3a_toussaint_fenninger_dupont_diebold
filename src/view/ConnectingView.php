<?php
/**
 * Created by PhpStorm.
 * User: redma
 * Date: 20/12/2017
 * Time: 16:45
 */

namespace mywishlist\view;


class ConnectingView extends AbstractView
{
    public function render(){
        return <<<END
    <section id ="connecting">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Connexion</h2>
          </div>
        </div>
<form method="POST" action="{$this->app->urlFor('connexion_valid')}">
  <div class="form-group">
    <label for="mail">Adresse e-mail</label>
    <input type="email" class="form-control" id="mail" name="mail" placeholder="Entrez votre adresse mail" autofocus>
  </div>
  <div class="form-group">
    <label for="password">Mot de passe</label>
    <input type="password" class="form-control" id="password" name="password" placeholder="Entrez votre mot de passe">
  </div>
  <button type="submit" class="btn btn-primary" name="connexion" value="connect">Connexion</button>
</form>
</div>
</section>
END;
    }
}