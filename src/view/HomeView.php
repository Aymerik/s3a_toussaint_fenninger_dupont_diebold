<?php
/**
 * Created by PhpStorm.
 * User: aymer
 * Date: 12/12/17
 * Time: 11:47
 */

namespace mywishlist\view;

use mywishlist\model\WishList;

class HomeView extends AbstractView
{
    public function render(){
        $listes = WishList::where('isPublic', '=', 1)->orderBy('deadline','asc')->take(6)->get();
        $listarr = [];
        foreach ($listes as $liste){

            $listarr[]=$liste;
        }
        for($i = count($listarr); $i<6 ;$i++){
            $list = new WishList();
            $list->title = 'Pas de liste publique supplémentaire';
            $list->desc = '/';
            $listarr[]= $list;
        }
        $return = <<<END
 <!-- Services -->
    <section id="sectionliste">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Services</h2>
            <h3 class="section-subheading text-muted">Découvrez les services offerts par notre site</h3>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Créez...</h4>
            <p class="text-muted">Vous fêtez votre anniversaire ? Un mariage ? Une soirée à organiser ? Créez la liste de tout ce qu'il vous faut !</p>
          </div>
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Partagez...</h4>
            <p class="text-muted">Partagez votre liste à tous vos amis par email ou par lien.</p>
          </div>
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Et choisissez !</h4>
            <p class="text-muted">Laissez vos amis sélectionner les items et regardez l'état de votre liste.</p>
          </div>
        </div>
      </div>
    </section>

    <!-- Contact -->
    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Contactez-nous</h2>
            <h3 class="section-subheading text-muted">Des questions ? Un problème sur le site ? Contactez-nous via ce formulaire !</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <form id="contactForm" name="sentMessage" method="POST" action="{$this->app->urlFor('contact')}" novalidate>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" id="name" type="text" placeholder="Votre nom *" required data-validation-required-message="Merci d'entrer votre nom" name="name">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="email" type="email" placeholder="Votre email *" required data-validation-required-message="Merci d'entrer votre adresse mail." name="email">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="subject" type="tel" placeholder="Sujet *" required data-validation-required-message="Merci de préciser un sujet." name="subject">
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <textarea class="form-control" id="message" placeholder="Votre Message *" required data-validation-required-message="Merci d'entre un message." name="message"></textarea>
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Envoyer !</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

END;
        return $return;
    }
}