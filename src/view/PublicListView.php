<?php

namespace mywishlist\view;


use mywishlist\model\WishList;

class PublicListView extends AbstractView
{

    public function render($listes)
    {
        $listes = $listes[0];
        $atLeast1 = false;
        $content = '';
        foreach ($listes as $liste) {
            if (strtotime($liste->deadline) > time()) {
                $atLeast1 = true;
                $urlList = $this->app->urlFor('liste', ['id' => $liste->id, 'token' => $liste->token]);
                $titre = $liste->title;
                $time = date('d/m/Y',strtotime($liste->deadline));
                $content .= <<<END
<a class="mylistlink" href="$urlList">
    <div class="normalMyList">
    <h4><strong>$titre</strong></h4> <h6>arrive à échéance le $time</h6>
    <p>{$liste->description} </p>
    </div>
</a>
END;
            }
        }
        if(!$atLeast1){
            $content = '<h3>Il n\'y a actuellement aucune liste publique d\'ouverte.</h3>';
        }
        return <<<END
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                $content
            </div>
        </div>
    </div>
</section>
END;

    }
}
