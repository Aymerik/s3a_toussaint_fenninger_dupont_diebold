<?php

namespace mywishlist\view;


use mywishlist\model\WishList;

class CreatorsView extends AbstractView
{

    public function render($users)
    {
        $content = '';
        foreach ($users as $user) {
                $titre = $user->username;
                $content .= <<<END
<div class="mylistlink">
    <div class="normalMyList">
    <h4><strong>$titre</strong></h4>
    </div>
</div>
END;
            }
        return <<<END
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                $content
            </div>
        </div>
    </div>
</section>
END;

    }
}
