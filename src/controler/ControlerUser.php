<?php
/**
 * Actions possibles pour un utilisateur
 */

namespace mywishlist\controler;

use mywishlist\model\Item;
use mywishlist\model\User;
use mywishlist\model\WishList;
use \mywishlist\view\GlobalView as View;
use Slim\Slim;


class ControlerUser extends AbstractControler
{
    public function __construct(){
        parent::__construct();
        if(empty($_SESSION['user']))
        {
            $this->app->halt(403, 'Vous ne passerez pas... !');
        }
    }

    public function mesListes(){
        echo (new View($_SESSION['user']))->render(View::AFF_MES_LISTES);
    }

    public function deconnexion(){
        unset($_SESSION['user']);
        $this->app->flash('message', 'Au revoir et à bientôt !');
        $this->app->redirect($this->app->urlFor("accueil"));
    }

    public function creerListe(){
        $v = new View();
        echo $v->render(View::AFF_CREER_LISTE);
    }

    public function creerListeValider(){
        if(!$this->app->request->post('name') != null || !$this->app->request->post('description') != null || !$this->app->request->post('deadline') != null)
        {
            $this->app->flash('message', 'Merci de remplir tous les champs');
            $this->app->redirect($this->app->urlFor('creer_liste'));
        }
        $token = bin2hex(random_bytes(32));

        $liste = new WishList();
        $liste->title = filter_var($this->app->request->post('name'), FILTER_SANITIZE_STRING);
        $liste->description = filter_var($this->app->request->post('description'), FILTER_SANITIZE_STRING);
        $liste->user_id = unserialize($_SESSION['user'])->id;
        $liste->deadline = filter_var($this->app->request->post('deadline'), FILTER_SANITIZE_SPECIAL_CHARS);
        $liste->token = $token;
        $liste->isPublic = $this->app->request->post('isPublic') != null;
        $liste->save();
        //met en cookie le token de la liste seulement si il est le createur
        if(isset($_COOKIE['liste'])){
            $arr = unserialize($_COOKIE['liste']);
        }
        $arr[$token]='c';
        setcookie('liste',serialize($arr));
        $this->app->redirect($this->app->urlFor('liste', ['id' => $liste->id, 'token' => $liste->token]));
    }

    public function modifierListe($idListe)
    {
        $liste = WishList::where('id', '=', $idListe)->first();
        if(empty($liste))
        {
            $this->app->flash('message', 'Cette liste n\'existe pas !');
            $this->app->redirect($this->app->urlFor('accueil'));
        }
        else if($liste->user_id != unserialize($_SESSION['user'])->id)
        {
            $this->app->flash('message', 'Vous devez être le créateur de liste pour editer une liste.');
            $this->app->redirect($this->app->urlFor('accueil'));
        }
        $v = new View($liste);
        echo $v->render(View::AFF_MODIFIER_LISTE);
    }

    public function modifierListeValider($idListe)
    {
        $liste = WishList::where('id', '=', $idListe)->first();
        if(empty($liste))
        {
            $this->app->flash('message', 'Cette liste n\'existe pas !');
            $this->app->redirect($this->app->urlFor('accueil'));
        }
        else if($liste->user_id != unserialize($_SESSION['user'])->id)
        {
            $this->app->flash('message', 'Vous devez être le créateur de liste pour editer une liste.');
            $this->app->redirect($this->app->urlFor('accueil'));
        }
        $liste->title = filter_var($this->app->request->post('name'), FILTER_SANITIZE_SPECIAL_CHARS);
        $liste->description = filter_var($this->app->request->post('description'), FILTER_SANITIZE_SPECIAL_CHARS);
        $liste->deadline = filter_var($this->app->request->post('deadline'), FILTER_SANITIZE_SPECIAL_CHARS);
        $liste->isPublic = $this->app->request->post('isPublic') != null;
        $liste->save();
        $this->app->redirect($this->app->urlFor('liste', ['id' => $liste->id, 'token' => $liste->token]));

    }

    public function supprimerListe($idListe)
    {
        $liste = WishList::where('id', '=', $idListe)->first();
        if(empty($liste))
        {
            $this->app->flash('message', 'Cette liste n\'existe pas !');
            $this->app->redirect($this->app->urlFor('accueil'));
        }
        else if($liste->user_id != unserialize($_SESSION['user'])->id)
        {
            $this->app->flash('message', 'Vous devez être le créateur de liste pour la supprimer.');
            $this->app->redirect($this->app->urlFor('accueil'));
        }
        else {
            $liste->delete();
            $this->app->flash('message', 'La liste a bien été supprimée');
            $this->app->redirect($this->app->urlFor('accueil'));

        }
    }
    public function ajoutItem($idListe)
    {
        $liste = WishList::where('id', '=', $idListe)->first();
        if (isset($_FILES['image']) AND $_FILES['image']['error'] == 0) {
            if ($_FILES['image']['size'] <= 100000000) {
                $infosfichier = pathinfo($_FILES['image']['name']);
                $extension_upload = $infosfichier['extension'];
                $extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png');
                if (in_array($extension_upload, $extensions_autorisees)) {
                    $tokenURL = bin2hex(random_bytes(32));
                    var_dump($_FILES);
                    move_uploaded_file($_FILES['image']['tmp_name'], 'imgBank/' . $tokenURL . '.' . $infosfichier['extension']);
                    $image = 'imgBank/' . $tokenURL . '.' . $infosfichier['extension'];
                }
            }
        }
        if(empty($liste))
        {
            $this->app->flash('message', 'Cette liste n\'existe pas !');
            $this->app->redirect($this->app->urlFor('accueil'));
        }
        else if($liste->user_id != unserialize($_SESSION['user'])->id)
        {
            $this->app->flash('message', 'Vous devez être le créateur de liste pour ajouter un item.');
            $this->app->redirect($this->app->urlFor('accueil'));
        }

        else if(!$this->app->request->post('name') != null || !$this->app->request->post('description') != null || !$this->app->request->post('price') != null || (!$this->app->request->post('image') != null && !isset($image)))
        {
            $this->app->flash('message', 'Merci de remplir au moins le nom, la description, le prix et l\'image de l\'item.');
            $this->app->redirect($this->app->urlFor('liste', ['id' => $idListe, 'token' => $liste->token]));
        }
        else{
            $item = new Item();
            $item->name = filter_var($this->app->request->post('name'), FILTER_SANITIZE_SPECIAL_CHARS);
            $item->description = filter_var($this->app->request->post('description'), FILTER_SANITIZE_SPECIAL_CHARS);
            $item->price = filter_var($this->app->request->post('price'), FILTER_SANITIZE_NUMBER_FLOAT);
            $item->images = filter_var(isset($image) ? $this->app->request->getRootUri().'/'.$image : $this->app->request->post('image'), FILTER_SANITIZE_URL);
            $item->link = filter_var($this->app->request->post('link'), FILTER_SANITIZE_SPECIAL_CHARS) ?? '';
            $item->list_id = $idListe;

            $item->save();
            $this->app->flash('message', 'Item ajouté avec succès');
            $this->app->redirect($this->app->urlFor('liste', ['id' => $idListe, 'token' => $liste->token]));

        }
    }

    public function profile(){
        $user = unserialize($_SESSION['user']);
        echo (new \mywishlist\view\GlobalView($user))->render(\mywishlist\view\GlobalView::AFF_PROFILE);
    }

    public function modifyProfile(){
        $username = $this->app->request->post('username');
        $newpass = $this->app->request->post('newpassword');
        $newpassv = $this->app->request->post('newpasswordvalid');
        $oldpass = $this->app->request->post('oldpassword');

        if(!isset($newpass)){
            $this->app->flash('warning','Il faut indiquer son ancien mot de passe obligatoirement');
            $this->app->redirect($this->app->urlFor('profile'));
        }else{
            $user = unserialize($_SESSION['user']);
            if(User::checkPassword($user->mail,$oldpass)){
                $success = false;
                $error = false;
                $diffnewpass = false;
                if(isset($username)){
                    if($user->username != $username){
                        if(self::checkUsernamePolitic($username)){
                            $user->username = filter_var($username, FILTER_SANITIZE_SPECIAL_CHARS);
                            $success = true;
                        }else{
                            $this->app->flash('warning','Le nom d\'utilisateur doit faire au moins 3 caractères');
                            $error = true;
                        }
                    }
                }else{
                    $error = true;
                    $this->app->flash('warning','Le nom d\'utilisateur doit faire au moins 3 caractères');
                }
                if(!$error && isset($newpass) && isset($newpassv)){
                    if($newpass != $newpassv){
                        $error = true;
                        $this->app->flash('warning','Merci de fournir un mot de passe de confirmation identique');
                    }
                    if(self::checkPasswordPolitic($newpass)){
                        $user->password = self::encodePassword($newpass);
                        $success = true;
                        $diffnewpass = true;
                    }else{
                        $error = true;
                        $this->app->flash('warning','Le mot de passe doit faire 7 caractères au minimum');
                    }
                }


                if($success && !$error){
                    $user->save();
                    $_SESSION['user'] = serialize($user);
                    $this->app->flash('message','Les modifications ont bien été prises en compte');
                    if($diffnewpass){
                        self::deconnexion();
                    }else{
                        $this->app->redirect($this->app->urlFor('profile'));
                    }

                }

                if($error){

                }

            }else{
                $this->app->flash('warning','Il y a une erreur dans le mot de passe');
            }
            $this->app->redirect($this->app->urlFor('profile'));
        }
    }

    public function deleteAccount(){
        $user = unserialize($_SESSION['user']);
        if($user->id > 0){
            $user->delete();
            unset($_SESSION['user']);
            $this->app->flash('message', 'Compte supprimé avec succès');
            $this->app->redirect($this->app->urlFor("accueil"));
        }
    }

    public function checkUsernamePolitic($username){
        if(strlen($username)>=3){
            return true;
        }
        return false;
    }

    public function checkPasswordPolitic($password){
        if(strlen($password)>=7){
            return true;
        }
        return false;
    }

    public function encodePassword($password){
        return password_hash($password,PASSWORD_BCRYPT);
    }

    public function liste($liste){
        if(unserialize($_SESSION['user'])->id == $liste->user_id){
            if(isset($_COOKIE['liste'])){
                $tokens = unserialize($_COOKIE['liste']);
                if(empty($tokens[$liste->token])){
                    $tokens[$liste->token]='c';
                    setcookie('liste',serialize($tokens));
                }
            }else{
                $tokens[$liste->token] = 'c';
                setcookie('liste',serialize($tokens));
            }

        }
    }

    public function modifItem(){
        if(!$this->app->request->post('id_list') != null || !$this->app->request->post('token') != null || !$this->app->request->post('id_item') != null){
            $this->app->redirect($this->app->urlFor('accueil'));
        }
        $idListe = $this->app->request->post('id_list');
        $token = $this->app->request->post('token');
        $idItem = $this->app->request->post('id_item');

        $liste = WishList::where('id', '=', $idListe)->first();
        if($liste->token != $token){
            $this->app->flash('message', 'Le token est erroné');
            $this->app->redirect($this->app->urlFor('accueil'));
        }else if(empty($liste))
        {
            $this->app->flash('message', 'Cette liste n\'existe pas !');
            $this->app->redirect($this->app->urlFor('accueil'));
        }
        else if($liste->user_id != unserialize($_SESSION['user'])->id)
        {
            $this->app->flash('message', 'Vous devez être le créateur de liste pour ajouter un item.');
            $this->app->redirect($this->app->urlFor('accueil'));
        }
        else if(!$this->app->request->post('name') != null || !$this->app->request->post('description') != null || !$this->app->request->post('price') != null)
        {
            $this->app->flash('message', 'Merci de remplir au moins le nom, la description et le prix de l\'item.');
            $this->app->redirect($this->app->urlFor('liste', ['id' => $idListe, 'token' => $liste->token]));
        }else if(!is_null(Item::where('id','=',$idItem)->first()->booking_id)){
            $this->app->flash('message', 'L\'item est déjà réservé et ne peut donc plus être modifié');
            $this->app->redirect($this->app->urlFor('item',['item'=>$idItem,'token'=>$token]));
        }else{
            $item = Item::where('id','=',$idItem)->first();
            $item->name = filter_var($this->app->request->post('name'), FILTER_SANITIZE_SPECIAL_CHARS);
            $item->description = filter_var($this->app->request->post('description'), FILTER_SANITIZE_SPECIAL_CHARS);
            $item->price = filter_var(str_replace(',','.',floatval($this->app->request->post('price'))), FILTER_SANITIZE_NUMBER_FLOAT);
            if($this->app->request->post('image') != null || isset($_FILES['image'])) {
                $image = '';
                if (isset($_FILES['image']) AND $_FILES['image']['error'] == 0) {
                    if ($_FILES['image']['size'] <= 100000000) {
                        $infosfichier = pathinfo($_FILES['image']['name']);
                        $extension_upload = $infosfichier['extension'];
                        $extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png');
                        if (in_array($extension_upload, $extensions_autorisees)) {
                            $tokenURL = bin2hex(random_bytes(32));
                            var_dump($_FILES);
                            move_uploaded_file($_FILES['image']['tmp_name'], 'imgBank/' . $tokenURL . '.' . $infosfichier['extension']);
                            $image = $this->app->request->getRootUri().'/imgBank/' . $tokenURL . '.' . $infosfichier['extension'];

                        }
                    }
                } else {
                    $image = $this->app->request->post('image');
                }
                var_dump($image);
                if($this->app->request->post('image') != null) var_dump($this->app->request->post('?1'));
                var_dump($_FILES['image']);
                exit (0);
                $item->images = filter_var($image, FILTER_SANITIZE_URL);

            }
            $item->link = filter_var($this->app->request->post('link'), FILTER_SANITIZE_SPECIAL_CHARS) ?? '';
            $item->list_id = $idListe;

            $item->save();
            $this->app->flash('message', 'Item modifié avec succès');
            $this->app->redirect($this->app->urlFor('liste', ['id' => $idListe, 'token' => $liste->token]));

        }
    }

    public function deleteItem($id,$idList,$token){
        if(unserialize($_SESSION['user'])->id == WishList::where([['id','=',$idList],['token','=',$token]])->first()->user_id){
            $item = Item::where('id','=',$id)->first();
            if($item->booking_id == null){
                $item->delete();
            }else{
                $this->app->flash('message', 'L\'item est déjà réservé et ne peut donc plus être supprimé');
                $this->app->redirect($this->app->urlFor('item',['item'=>$id,'token'=>$token]));
            }
            $this->app->redirect($this->app->urlFor('liste',['id'=>$idList,'token'=>$token]));
        }
        $this->app->flash('message', 'Erreur dans l\'url');
        $this->app->redirect($this->app->urlFor('accueil'));
    }


    public function deleteImgItem($id,$idList,$token){
        if(unserialize($_SESSION['user'])->id == WishList::where([['id','=',$idList],['token','=',$token]])->first()->user_id){
            $item = Item::where('id','=',$id)->first();
            if($item->booking_id == null){
                $item->images = "";
                $item->save();
            }else{
                $this->app->flash('message', 'L\'item est déjà réservé et ne peut donc plus être modifié');
                $this->app->redirect($this->app->urlFor('item',['item'=>$id,'token'=>$token]));
            }
            $this->app->redirect($this->app->urlFor('liste',['id'=>$idList,'token'=>$token]));
        }
        $this->app->flash('message', 'Erreur dans l\'url');
        $this->app->redirect($this->app->urlFor('accueil'));
    }

    public function activity(){
        $user = unserialize($_SESSION['user']);
    }

}