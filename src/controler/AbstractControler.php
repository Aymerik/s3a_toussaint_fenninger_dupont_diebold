<?php

namespace mywishlist\controler;


abstract class AbstractControler
{

    protected $app;

    public function __construct(){
        $this->app = \Slim\Slim::getInstance();
        if(isset($_SESSION['user']))
        {
            // on rafraichit la session...
            $_SESSION['user'] = serialize(\mywishlist\model\User::where('mail', '=', unserialize($_SESSION['user'])->mail)->first());
        }
    }

}