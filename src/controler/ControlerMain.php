<?php
/**
 * Created by PhpStorm.
 * User: aymer
 * Date: 12/12/2017
 * Time: 11:46
 */

namespace mywishlist\controler;

use Illuminate\Database\Capsule\Manager;
use mywishlist\model\Booking;
use mywishlist\model\Comment;
use mywishlist\model\Item;
use mywishlist\model\User;
use mywishlist\model\WishList;
use \mywishlist\view\GlobalView as View;

class ControlerMain extends AbstractControler
{
    public function index(){
        $v = new View();
        echo $v->render(View::AFF_HOME);
    }

    public function contact(){

        if(isset($_POST['name']) && isset($_POST['email']) && isset($_POST['subject']) && isset($_POST['message']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
        {
            $name = filter_var($_POST['name'], FILTER_SANITIZE_SPECIAL_CHARS);
            $title = filter_var($_POST['subject'], FILTER_SANITIZE_SPECIAL_CHARS);
            $content = filter_var($_POST['message'], FILTER_SANITIZE_SPECIAL_CHARS);
            $email = $_POST['email'];
            $to      = 'diebold.aymerik@gmail.com';
            $subject = '[Contact MyWishList] '.$title;
            $message = $name. ' vous a contacté à ce sujet : <strong>'.$title.'</strong><br />Voici son message :<br /><br />'.$content;
            $headers = 'From: '.$email . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
            if(mail($to, $subject, $message, $headers))
                $this->app->flash('message', 'Votre message a bien été envoyé et sera traité dans les meilleurs délais');
            else
                $this->app->flash('message', '<strong>Erreur</strong> : Votre message n\'a pas pu être envoyé. Merci de réessayer');
            $this->app->redirect($this->app->urlFor('accueil'));
        }else{
           $this->app->flash('message', '<strong>Erreur</strong> : Merci de remplir tous les champs et de donner une adresse email valide');
           $this->app->redirect($this->app->urlFor('accueil'));
       }
   }

   public function connexion(){
    if(isset($_SESSION['user'])){
        $this->app->redirect($this->app->urlFor("accueil"));
    }

    $v = new View();
    echo $v->render(View::AFF_CONNEXION);
}

public function register(){
    if(isset($_SESSION['user'])){
        $this->app->redirect($this->app->urlFor("accueil"));
    }
    $v = new View();
    echo $v->render(View::AFF_REGISTER);
}

public function registerValid()
{
    if(isset($_SESSION['user'])){
        $this->app->redirect($this->app->urlFor("accueil"));
    }

    if(!isset($_POST['username']) || !isset($_POST['password']) || !isset($_POST['confirm']) || !isset($_POST['mail']))
    {
        $this->app->flash('message', 'Merci de remplir tous les champs');
    }
    else if(strlen($_POST['password']) < 7)
        $this->app->flash('message', 'Le mot de passe doit au moins faire 7 caractères');
    else if($_POST['confirm'] != $_POST['password'])
        $this->app->flash('message', 'Les deux mots de passes ne correspondent pas');
    else if(!filter_var($_POST['mail'],FILTER_VALIDATE_EMAIL))
        $this->app->flash('message', 'L\'adresse e-mail n\'est pas valide !');
    else
    {
        $username = filter_var($_POST['username'], FILTER_SANITIZE_SPECIAL_CHARS);
        if(\mywishlist\model\User::where('mail', '=', $_POST['mail'])->count() > 0)
            $this->app->flash('message', 'Cette adresse est déjà utilisée');
        else
        {
            $user = new \mywishlist\model\User();
            $user->username = $username;
            $user->mail = $_POST['mail'];
            $user->password = password_hash($_POST['password'], PASSWORD_DEFAULT);
            $user->save();
            $this->app->flash('message', 'Votre compte a bien été créé ! Vous pouvez maitenant vous connecter !');
        }
    }
    $this->app->redirect($this->app->urlFor('register'));

}

public function connexionValid(){
    if(isset($_SESSION['user'])){
        $this->app->redirect($this->app->urlFor("accueil"));
    }

    if(!isset($_POST['mail']) || !isset($_POST['mail']))
    {
        $this->app->flash('message', 'Merci de remplir tous les champs');
    }
    else if((($user = \mywishlist\model\User::where('mail', '=', $_POST['mail'])->first()) ?? null) == null)
    {
        $this->app->flash('message', 'Cette adresse mail n\'existe pas dans notre base');
    }
    else if(!password_verify($_POST['password'], $user->password))
    {
        $this->app->flash('message', 'Le mot de passe est incorrect');
    }
    else
    {
        $_SESSION['user'] = serialize($user);
        $this->app->flash('message', 'Bienvenue '.$user->username.' !');
        $this->app->redirect($this->app->urlFor('accueil'));
    }
    $this->app->redirect($this->app->urlFor('connexion'));
}

    public function liste($id, $token)
    {
        $liste = \mywishlist\model\WishList::where([
            ['id', '=', $id],
            ['token', '=', $token]
        ])->first();

        if (empty($liste)) {
            $this->app->flash('message', 'Cette liste n\'existe pas');
            $this->app->redirect($this->app->urlFor('accueil'));
        } else {
            if(isset($_SESSION['user'])){
                (new ControlerUser)->liste($liste);
            }
            $v = new View($liste);
            echo $v->render(View::AFF_LISTE);
        }
    }

    public function listePoster($id, $token)
    {
        $liste = \mywishlist\model\WishList::where([
            ['id', '=', $id],
            ['token', '=', $token]
        ])->first();

        if (empty($liste)) {
            $this->app->flash('message', 'Cette liste n\'existe pas');
            $this->app->redirect($this->app->urlFor('accueil'));
        } else {
            if($this->app->request->post('author') !== null && $this->app->request->post('content') !== null){
                $comment = new Comment();
                $comment->author = filter_var($this->app->request->post('author'), FILTER_SANITIZE_SPECIAL_CHARS);
                $comment->content = filter_var($this->app->request->post('content'), FILTER_SANITIZE_SPECIAL_CHARS);
                $comment->list_id = $liste->id;
                $comment->created_at = date('Y-m-d H:i:s');
                $comment->save();
                $this->app->flash('message', 'Commentaire publié !');

            }
            else
            {
                $this->app->flash('message', 'Merci de remplir tous les champs.');
            }
            $this->app->redirect($this->app->urlFor('liste', [
                'id' => $id,
                'token' => $token
            ]));
        }
    }


    public function afficherItem($id, $token)
    {
        $item = \mywishlist\model\Item::where('id', '=', $id)->first();

        if (empty($item)) {
            $this->app->flash('message', 'Cet objet n\'existe pas');
            $this->app->redirect($this->app->urlFor('accueil'));
        } else {
            $liste = WishList::where([['id', '=', $item->list_id], ['token', '=', $token]])->first();
            if (empty($liste)) {
                $this->app->flash('message', 'Cet objet n\'exite pas dans cette liste');
                $this->app->redirect($this->app->urlFor('accueil'));
            } else {
                $arr = array("item"=>serialize($item),"token"=>$token, "idList" => $liste->id);
                $v = new \mywishlist\view\GlobalView($arr);
                echo $v->render(\mywishlist\view\GlobalView::AFF_ITEM);
            }
        }
    }

    public function modifierItem($id, $token){
        $item = \mywishlist\model\Item::where('id', '=', $id)->first();

        if (empty($item)) {
            $this->app->flash('message', 'Cet objet n\'existe pas');
            $this->app->redirect($this->app->urlFor('accueil'));
        } else {
            $liste = WishList::where([['id', '=', $item->list_id], ['token', '=', $token]])->first();
                $this->app->flash('message', 'Cet objet n\'exite pas dans cette liste');
                $this->app->redirect($this->app->urlFor('accueil'));



                $arr = array("item"=>$item,"token"=>$token);
                $v = new \mywishlist\view\GlobalView($arr);
                echo $v->render(\mywishlist\view\GlobalView::AFF_ITEM);

        }
    }


    public function view_list_pub(){
        $listes = WishList::where('isPublic', '=', 1)->orderBy('deadline','asc')->get();
        echo (new View([$listes]))->render(View::AFF_PUBLIC_LISTES);
    }



    public function bookItem($id){
        if($this->app->request->post('id_list') === null || $this->app->request->post('token') === null)
        {
            $this->app->flash('message', 'Utilisation non conventionnelle');
            $this->app->redirect($this->app->urlFor('accueil'));
        }
        else if($this->app->request->post('id_list') !== null && $this->app->request->post('token') !== null && $this->app->request->post('name') !== null &&$this->app->request->post('comment') !== null){
            $liste = WishList::where([
                ['id', '=', (int)$this->app->request->post('id_list')],
                ['token', '=', $this->app->request->post('token')]
            ])->first();
            if(empty($liste))
            {
                $this->app->flash('message', 'Cette liste n\'existe pas');
                $this->app->redirect($this->app->urlFor('accueil'));
            }
            $item = Item::where([
                ['id', '=', $id],
                ['list_id', '=', (int)$this->app->request->post('id_list')]
            ])->first();
            if(empty($item))
            {
                $this->app->flash('message', 'Cet item n\'existe pas.');
                $this->app->redirect($this->app->urlFor('accueil'));
            }

            if(!is_null($item->booking_id)){
                $this->app->flash('message', 'Cet item est déjà réservé');
                $this->app->redirect($this->app->urlFor('liste', ['id' => $this->app->request->post('id_list'), 'token' => $this->app->request->post('token')]));
            }

            $book = new Booking();
            $book->name = htmlspecialchars($this->app->request->post('name'));
            $book->comment = htmlspecialchars($this->app->request->post('comment'));
            $book->created_at = date('Y-m-d H:i:s');
            $book->save();

            $item->booking_id = $book->id;
            $item->save();
            $this->app->flash('message', 'Item réservé ! Merci !');
            $this->app->redirect($this->app->urlFor('liste', ['id' => $this->app->request->post('id_list'), 'token' => $this->app->request->post('token')]));

        }else{
            $this->app->flash('message', 'Merci de remplir tous les champs');
            $this->app->redirect($this->app->urlFor('liste', ['id' => $this->app->request->post('id_list'), 'token' => $this->app->request->post('token')]));
        }
    }

    public function creators(){
        $users = Manager::table('user')
                ->select('username')
                ->join('list', 'user.id', '=', 'list.user_id')
                ->where('list.isPublic','=',true)
                ->groupBy('username')
                ->havingRaw('COUNT(list.id) > 0')
                ->get();
        echo (new View($users))->render(View::AFF_CREATORS);
    }
}
