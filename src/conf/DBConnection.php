<?php
/**
 * Created by PhpStorm.
 * User: redma
 * Date: 12/12/2017
 * Time: 13:16
 */

namespace mywishlist\conf;


class DBConnection
{
    public static function initDB($file){
        $db = new \Illuminate\Database\Capsule\Manager();
        $db->addConnection(parse_ini_file($file));
        $db->setAsGlobal();
        $db->bootEloquent();
    }
}