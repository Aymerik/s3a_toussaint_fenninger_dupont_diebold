<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 18/12/2017
 * Time: 10:47
 */

namespace mywishlist\model;


class Authentification
{
    private static $profile;

    public static function createUser($u,$p,$e){
        $user = User::where('username','=',$u);
        if($user != null || $p==null || (User::where('email','=',$e))==null){
            throw new \Exception();
        }else{
            $pass = password_hash($p);
            $utilisateur = new \mywishlist\models\Utilisateur();
            $utilisateur->password = $pass;
            $utilisateur->username = $u;
            $utilisateur->email = $e;
            $utilisateur->save();
            $utilisateur->role = 10;
        }
    }

    public static function checkUser($username,$password){
        $hashPass = password_hash($password);
        $user = User::where('username','=',$username)->first();
        if($user == null) {
            throw new \Exception();
        }
        if(password_verify($user->password,$hashPass)){
            static::$profile['email'] = $user->email;
            static::$profile['username'] = $user->username;
            static::$profile['role'] = $user->role;
        }else{
            throw new \Exception();
        }
    }

    public static function loadProfile(){
    $_SESSION['profile']=null;
    $_SESSION['profile'] = static::$profile;

    }

    public static function checkAuth($auth){
        return (static::$profile['role'] >= $auth);
    }



}