<?php
/**
 * Created by PhpStorm.
 * User: redma
 * Date: 11/12/2017
 * Time: 11:43
 */

namespace mywishlist\model;


class WishList extends \Illuminate\Database\Eloquent\Model
{
    protected $primaryKey = 'id';
    protected $table = 'list';
    public $timestamps = false;

    public function items(){
        return $this->hasMany('\mywishlist\model\Item','list_id');
    }

    public function comments(){
        return $this->hasMany('\mywishlist\model\Comment', 'list_id');
    }

    public function isFinished(){
        return strtotime($this->deadline) < time();
    }
}