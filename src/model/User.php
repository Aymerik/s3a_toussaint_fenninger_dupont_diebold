<?php
/**
 * Created by PhpStorm.
 * User: redma
 * Date: 11/12/2017
 * Time: 11:44
 */

namespace mywishlist\model;


class User extends \Illuminate\Database\Eloquent\Model
{
    protected $primaryKey = 'id';
    protected $table = 'user';
    public $timestamps = false;

    public static function checkPassword($email,$pass){
        password_hash($pass,PASSWORD_BCRYPT);
        $user = self::where('mail','=',$email)->first();
        if($user != null){
            return password_verify($pass,$user->password);
        }else{
            return false;
        }
    }

}