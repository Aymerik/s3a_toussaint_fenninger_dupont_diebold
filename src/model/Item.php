<?php
/**
 * Created by PhpStorm.
 * User: lex
 * Date: 21/11/17
 * Time: 10:35
 */

namespace mywishlist\model;

class Item extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'item';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function booking(){
        return $this->belongsTo('\mywishlist\model\Booking', 'booking_id');
    }
}