<?php
/**
 * Created by PhpStorm.
 * User: redma
 * Date: 11/12/2017
 * Time: 11:44
 */

namespace mywishlist\model;


class Booking extends \Illuminate\Database\Eloquent\Model
{
    protected $primaryKey = 'id';
    protected $table = 'booking';
    public $timestamps = false;
}