<?php
/**
 * Created by PhpStorm.
 * User: redma
 * Date: 11/12/2017
 * Time: 11:44
 */

namespace mywishlist\model;


class Comment extends \Illuminate\Database\Eloquent\Model
{
    protected $primaryKey = 'id';
    protected $table = 'comment';
    public $timestamps = false;
}