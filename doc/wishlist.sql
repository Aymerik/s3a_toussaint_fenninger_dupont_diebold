-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 17 jan. 2018 à 19:54
-- Version du serveur :  5.7.19
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `wishlist`
--

-- --------------------------------------------------------

--
-- Structure de la table `booking`
--

DROP TABLE IF EXISTS `booking`;
CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `booking`
--

INSERT INTO `booking` (`id`, `name`, `comment`, `created_at`) VALUES
(1, 'Jean Marcel', 'Ca me coûte un bras, mais c\'est bien parce que c\'est toi !', '2018-01-17 19:52:02'),
(2, 'Aimé-rené', 'J\'espère que ça te fera plaisir !', '2018-01-17 19:52:43');

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `content` text NOT NULL,
  `list_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_list_in_comment` (`list_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`id`, `author`, `created_at`, `content`, `list_id`) VALUES
(1, 'Chlomu', '2018-01-17 13:32:15', 'La joie', 9),
(2, 'Aydi', '2018-01-17 14:29:01', 'Moi j&#39;aime pas apple', 10),
(3, 'Jean Martin', '2018-01-17 19:42:15', 'Sympa cette wishlist !', 8);

-- --------------------------------------------------------

--
-- Structure de la table `item`
--

DROP TABLE IF EXISTS `item`;
CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `description` text NOT NULL,
  `link` varchar(255) NOT NULL,
  `images` text NOT NULL,
  `booking_id` int(11) DEFAULT NULL,
  `list_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_booking_in_item` (`booking_id`),
  KEY `fk_list_in_item` (`list_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `item`
--

INSERT INTO `item` (`id`, `name`, `price`, `description`, `link`, `images`, `booking_id`, `list_id`) VALUES
(6, 'Yatch', '85000000.00', 'Un yatch', '', 'http://www.aquamarine.fr/specific_images/9/2/5/ketos-48-569.jpg?update=20170712102454', NULL, 9),
(7, 'île Privée', '3900000.00', 'Un petit île privée pour moi toute seule', '', 'https://img-0.journaldunet.com/65RqRzs2mkv7Bli4zrtXQOdU1Nw=/1240x/smart/c321f9ccfbea48bb8e4230a1299e565f/ccmcms-jdn/10100746-st-athanisios.jpg', NULL, 9),
(8, 'Villa', '1335000.00', 'il faut une villa sur une île privée', 'http://www.seloger.com/annonces/achat-de-prestige/maison/nantes-44/chantenay-sainte-anne/121391491.htm', 'https://v.seloger.com/s/width/799/visuels/0/s/p/o/0spo8fg8n31au9dz9y7x09bczdmpnfh4ihvsjjlg3.jpg', NULL, 9),
(9, 'cuisinière', '299.00', 'Pour la cuisine', '', 'https://www.electrodepot.fr/media/catalog/category/Cuisiniere-gaz.jpg', NULL, 8),
(10, 'Lave Vaisselle', '365.00', 'Pour la cuisine', '', 'https://media.conforama.fr/Medias/500000/70000/5000/000/70/G_575074_A.jpg', 2, 8),
(11, 'Sérpillère', '52.00', 'Pour laver le sol', 'http://www.priceminister.com/offer/buy/198618722/balai-serpillere-twist-mop-essorage-magique.html', 'https://pmcdn.priceminister.com/photo/balai-serpillere-twist-mop-essorage-magique-937655430_L.jpg', NULL, 8),
(12, 'Lave Linge', '299.00', 'Pour laver le linge', '', 'https://boulanger.scene7.com/is/image/Boulanger/bfr_overlay?layer=comp&$t1=&$product_id=Boulanger/8050147008911_h_f_l_0&wid=400&hei=400', NULL, 8),
(13, 'Iphone 7', '518.00', 'j&#39;aime apple &#60;3&#60;3&#60;3', '', 'https://pmcdn.priceminister.com/photo/1107577371.jpg', NULL, 10),
(14, 'MacBook Pro', '1523.00', 'J&#39;aime apple &#60;3&#60;3&#60;3', '', 'https://www.electrodepot.fr/media/catalog/product/cache/image/800x/beff4985b56e3afdbeabfc89641a4582/P957170.jpg', NULL, 10),
(15, 'Ipod', '2955.00', 'J&#39;aime apple &#60;3&#60;3&#60;3', '', 'https://pmcdn.priceminister.com/photo/apple-ipod-touch-5g-64-go-bleu-1145233491_ML.jpg', 1, 10),
(17, 'Console de jeu', '250.00', 'Une console de jeu pour égayer mes soirées !', '', '/s3a_toussaint_fenninger_dupont_diebold/imgBank/c1ec1fcf3974cf7fa8d32f8ad0e525615ee70416e409f46bbff9a2d5b0513931.png', NULL, 9);

-- --------------------------------------------------------

--
-- Structure de la table `list`
--

DROP TABLE IF EXISTS `list`;
CREATE TABLE IF NOT EXISTS `list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `deadline` date NOT NULL,
  `token` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `isPublic` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_user_in_list` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `list`
--

INSERT INTO `list` (`id`, `title`, `description`, `deadline`, `token`, `user_id`, `isPublic`) VALUES
(8, 'Mon Anniversaire', 'Liste Pour mon anniv', '2018-05-02', '569d6453dcaa5c0422d6b08f35df4d674c491764f648473535c1e4c4abff1148', 4, 1),
(9, 'Dream', 'Beaucoup de choses de rêves', '2050-01-01', '03cc1c08390a6a0806343f6822cb50eb59f5f362672475da9916ed53c8a37ce7', 4, 0),
(10, 'iLove', 'J&#39;aime tellement apple', '2999-09-09', '7aeb1f061b9f2b568a5c6a05b39a5766814dc1e1ce14e734162edb05b6ee3779', 5, 1);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `mail`, `username`, `password`) VALUES
(3, 'JeanDenis.Arin@gmail.com', 'JDArin', '$2y$10$mYws5FtUtKhA6hGO6nMhQOWqMW5.E/Bs8enSXZIUBAXlfCRlNB9KW'),
(4, 'Chloe.muronni@laposte.com', 'Chlomu', '$2y$10$RkAV5VEMu8KnJn01cimZYu.JeZkKk7jOOcfZIEO2wtM73zikuMsEK'),
(5, 'jj54@icloud.com', 'jj54', '$2y$10$RXLkqevCtahUVyVBfQjJXedRDZHtt4pemrllE3/q3Mx4WN72H2wkC'),
(6, 'Aymeric.Diebold@yopmail.tsn', 'Aydi', '$2y$10$OOhh1vda5Vfhz9ubA3Mgf.f3HxWnO.ck4L1Do8CgStsoTvb0JmQZ.');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `fk_list_in_comment` FOREIGN KEY (`list_id`) REFERENCES `list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `fk_booking_in_item` FOREIGN KEY (`booking_id`) REFERENCES `booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_list_in_item` FOREIGN KEY (`list_id`) REFERENCES `list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `list`
--
ALTER TABLE `list`
  ADD CONSTRAINT `fk_user_in_list` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
