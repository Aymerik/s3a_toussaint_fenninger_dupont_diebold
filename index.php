<?php
/**
 * Created by PhpStorm.
 * User: redma
 * Date: 05/12/2017
 * Time: 10:51
 */

session_start();

require_once __DIR__."/vendor/autoload.php";
use mywishlist\controler\ControlerMain as Main;
use mywishlist\controler\ControlerUser as User;

mywishlist\conf\DBConnection::initDB("src/conf/db.ini");
$app = new \Slim\Slim();


$app->get('/',function(){
  $controler = new mywishlist\controler\ControlerMain();
  $controler->index();
})->name("accueil");

$app->get('/meslistes',function(){
    (new User())->mesListes();
})->name("mesListes");

$app->get('/listes/:id/:token',function($id,$token){
    (new Main())->liste($id, $token);
})->name("liste");

$app->post('/liste/:id/:token/post', function($id, $token){
    (new Main())->listePoster($id, $token);
})->name('commenter_liste');

$app->get('/liste/creer', function(){
    (new User())->creerListe();
})->name('creer_liste');

$app->get('/liste/:id/supprimer', function($id){
    (new User())->supprimerListe($id);
})->name('supprimer_liste');

$app->get('/liste/:id/editer', function($id){
    (new User())->modifierListe($id);
})->name('editer_liste');

$app->post('/item/:id/book', function($id){
    (new \mywishlist\controler\ControlerMain())->bookItem($id);
})->name('book_item');

$app->get('/item/supprimer/:id/:idList/:token',function($id,$idList,$token){
    (new User())->deleteItem($id,$idList,$token);
})->name('suppr_item');

$app->get('/item/supprimer/image/:id/:idList/:token', function($id, $idList, $token){
    (new User())->deleteImgItem($id, $idList, $token);
})->name('suppr_img_item');

$app->get('/creators', function(){
    (new Main())->creators();
})->name('creators');

$app->post('/item/modif',function(){
    (new User())->modifItem();
})->name('modif_item');

$app->post('/liste/:id/editer', function($id){
    (new User())->modifierListeValider($id);
})->name('modifier_liste_valider');

$app->post('/liste/creer', function(){
    (new User())->creerListeValider();
})->name('creer_liste_valider');

$app->get('/item/:token/:item',function($token,$item){
    (new Main())->afficherItem($item, $token);
})->name("item");



$app->post('/liste/:id/item/add',function($id){
    (new User())->ajoutItem($id);

})->name('ajouter_item');

$app->get('/deconnexion',function(){
    (new User())->deconnexion();
})->name('deconnexion');

$app->get('/connexion',function(){
    (new Main())->connexion();
})->name("connexion");

$app->post('/connexion',function(){
    (new Main())->connexionValid();
})->name('connexion_valid');

$app->get('/register', function(){
    (new Main())->register();
})->name('register');

$app->post('/register', function(){
    (new Main())->registerValid();
})->name('register_valid');

$app->post('/contact', function(){
  (new Main())->contact();
})->name('contact');


$app->get('/profil',function(){
    (new User())->profile();
})->name('profile');

$app->post('/profil',function(){
    (new User)->modifyProfile();
});

$app->get('/delete',function(){
    (new User)->deleteAccount();
})->name('delete');

$app->get('/list_pub', function(){
    (new Main())->view_list_pub();
})->name('list_pub');
$app->run();
